package controller;

import java.util.Scanner;

public class MainController {

	public static void main(String[] args) {
		// Je declare l'Objet Scanner et je l'instencie
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Quel est votre age ?");
		int age;
		age = sc.nextInt();
		System.out.println("Votre age est :"+age);

		System.out.println("Quel est votre ville ?");
		String ville;
		ville = sc.next();
		System.out.println("Votre ville est :"+ville);
		
		System.out.println("Quel est votre poids ?");
		float poids;
		poids = sc.nextFloat();
		System.out.println("Votre poids est :"+poids);
	}

}
