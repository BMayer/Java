package controller;

public class TestController {

	public static void main(String[] args) {
		float poids = 110.0f; // en kg
		float taille = 1.8f; //en m
		float imc = poids / (taille * taille);
		if (imc < 18.5) {
			System.out.println("maigreur");
		}
		else if(imc< 25) { // 18.5 < imc < 25
			System.out.println("normal");
		}else {
			// imc correct 25
			float poidsIdeal = 25f*taille*taille;
			float kilo = poids -poidsIdeal;
			System.out.println("suproids");
			System.out.println("Vous devez perdre " + kilo + "kg");
		}

	}

}
