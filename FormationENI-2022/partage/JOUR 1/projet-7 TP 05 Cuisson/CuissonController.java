package controller;

import java.util.Scanner;

public class CuissonController {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Viande : 1 Boeuf - 2 Porc");
		int viande = sc.nextInt();
		System.out.println("Cuisson : 1 BLEU - 2 A POINT - 3 BIEN CUIT");
		int cuisson = sc.nextInt();
		System.out.println("poids en g. :");
		int poids = sc.nextInt();
		
		float coef =0.0f;
		if(viande == 1) { // BOEUF
			if(cuisson == 1) { //BLEU
				coef= 10.0f/500.0f;
			}
			else if(cuisson == 2) { //A POINT
				coef= 17.0f/500.0f;
			}
			else if(cuisson == 3) { //BIEN CUIT
				coef= 25.0f/500.0f;
			}

		}
		else if(viande == 2) { // PORC
			if(cuisson == 1) { //BLEU
				coef= 15.0f/400.0f;
			}
			else if(cuisson == 2) { //A POINT
				coef= 25.0f/400.0f;
			}
			else if(cuisson == 3) { //BIEN CUIT
				coef= 40.0f/400.0f;
			}

		}
		float tps = poids * coef * 60.0f; // *60 : en secondes
		int tps2 = (int) tps; 
		System.out.println("Votre temps de cuisson :"+tps2+" secondes");

		
	}

}
