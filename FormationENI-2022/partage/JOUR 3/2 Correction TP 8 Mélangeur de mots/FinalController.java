package controller;

import java.util.Random;

public class FinalController {

	public static void main(String[] args) {
		Random rd = new Random();
		int alea;
		String phrase = "notre cerveau est formidable";
		String phrase2 = "";
		String mot2;
		String[] tabMot = phrase.split(" ");
		for (String mot : tabMot) {
			mot2="";
			//String mot = "bonjour"; // taille ou length = 7
			// donc i va de 0 � 6
			char[] tabChar = mot.toCharArray();
			char[] tabChar2 = new char[tabChar.length];// Cr�er le tableau 2 "jumeaux"
			tabChar2[0] = tabChar[0];// Copier la 1ere lettre vers le tableau 2
			// Copier la Derni�re lettre dans le tableau 2
			tabChar2[tabChar.length - 1] = tabChar[tabChar.length - 1];
			// parcourir le tableau de 1 � 5
			for (int i = 1; i < tabChar.length - 1; i++) {
				// le nb aleatoire 1 � 5
				// alea = rd.nextInt(5); // de 0 � 4
				// alea = rd.nextInt(5)+1; // de 1 � 5
				// 5 c'est ?? tabChar.length-2
				do // Genere le nb aleatoire
				{
					alea = rd.nextInt(tabChar.length - 2) + 1; // de 1 � 5
				} while (tabChar2[alea] != '\u0000'); // tant que l'emplacement n'est pas vide

				// je viens mettre la lettre dans le tableau 2
				// tabChar2 <- tabChar
				tabChar2[alea] = tabChar[i];
				//System.out.println(tabChar[i]);
			}
			// pour chaque caractere du tableau 2
			// construire une chaine de caracteres
			for(char c: tabChar2)
			{
				mot2 +=c;
			}
			//System.out.println(tabChar2);
			//phrase2 += " "+tabChar2;
			phrase2 = phrase2 + " "+mot2;
		}
		// afficher la phrase 2
		System.out.println(phrase2);
	}

}
