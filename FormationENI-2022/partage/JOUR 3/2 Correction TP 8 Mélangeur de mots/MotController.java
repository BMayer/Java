package controller;

import java.util.Random;

public class MotController {

	public static void main(String[] args) {
		Random rd = new Random();
		int alea;
		// ---------------------------------------------------
		// Etape 1 :
		// ---------------------------------------------------
		// Transformer "bonjour" en tableau de caract�re
		String mot = "bonjour"; // taille ou length = 7
		// donc i va de 0 � 6
		char[] tabChar = mot.toCharArray();
		// System.out.println(tabChar); // afficher tableau
		// ---------------------------------------------------

		// ---------------------------------------------------
		// Etape 2 :
		// ---------------------------------------------------
		// Afficher les caractere 1 par 1 avec une boucle for ou foreach
		// ---------------------------------------------------
		for (int indice = 0; indice < tabChar.length; indice++) {
			System.out.println(tabChar[indice]);
		}
		// ---------------------------------------------------

		// ---------------------------------------------------
		// Etape 3 :
		// ---------------------------------------------------
		// Cr�er le tableau 2 "jumeaux" du tableau de caract�res
		// qui va acceuillir le mot m�lang�
		// Ce tableau est vide !
		// ---------------------------------------------------
		char[] tabChar2 = new char[tabChar.length];
		// ---------------------------------------------------

		// ---------------------------------------------------
		// Etape 4 :
		// ---------------------------------------------------
		// Copier la 1ere lettre vers le tableau 2
		// ---------------------------------------------------
		// tabChar2 <- tabChar
		tabChar2[0] = tabChar[0];
		// ---------------------------------------------------

		// ---------------------------------------------------
		// Etape 5 :
		// ---------------------------------------------------
		// Copier la Derni�re lettre dans le tableau 2
		// ---------------------------------------------------
		// tabChar2 <- tabChar
		tabChar2[tabChar.length - 1] = tabChar[tabChar.length - 1];
		// ---------------------------------------------------

		// v�rifier le contenu de tabChar 2
		System.out.println(tabChar2);
		System.out.println("-----"); // ligne pr la console
		
		// ---------------------------------------------------
		// Etape 6 :
		// ---------------------------------------------------
		// Parcourir le tableau de l'indice 1 � l'avant dernier indice
		// ---------------------------------------------------
		// indice va de 1 � 5
		// i va de 1 � tabChar.length-1
		for(int i=1;i<tabChar.length-1;i++)
		{
			System.out.println(tabChar[i]);
		}
		// ---------------------------------------------------
		
		// ---------------------------------------------------
		// Etape 7 :
		// ---------------------------------------------------
		// Choisir un emplacement au hazard dans le 2eme tableau
		// ---------------------------------------------------
		// indice va de 1 � 5
		// i va de 1 � tabChar.length-1
		for(int i=1;i<tabChar.length-1;i++)
		{
			// le nb aleatoire 1 � 5
			//alea = rd.nextInt(5); // de 0 � 4
			//alea = rd.nextInt(5)+1; // de 1 � 5
			// 5 c'est ?? tabChar.length-2
			alea = rd.nextInt(tabChar.length-2)+1; // de 1 � 5
			// je viens mettre la lettre dans le tableau 2
			// tabChar2 <- tabChar
			tabChar2[alea] = tabChar[i];
			System.out.println(tabChar[i]);
		}
		// ---------------------------------------------------	

		// ---------------------------------------------------
		// Etape 8 :
		// ---------------------------------------------------
		// Afficher le mot m�langer
		// ---------------------------------------------------
		System.out.println(tabChar2);
	}
}