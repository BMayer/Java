package controller;

import java.util.Scanner;

public class CookController {
	// zone attribut ou propri�t�
	// Variable de class
	private static Scanner sc;
	public static void main(String[] args) 
	{
		sc = new Scanner(System.in);
		int viande = saisirTypeViande();
		int cuisson = saisirTypeCuisson();
		int poids = saisirPoids();
		int tps = calculerTemps(viande,cuisson,poids);
		System.out.println("Votre temps de cuisson :"+tps+" secondes");
	
	}
	public static int saisirTypeViande() {
		System.out.println("Viande : 1 Boeuf - 2 Porc");
		int viande = sc.nextInt();
		return viande;
	}
	public static int saisirTypeCuisson() {
		System.out.println("Cuisson : 1 BLEU - 2 A POINT - 3 BIEN CUIT");
		int cuisson = sc.nextInt();
		return cuisson;
	}
	public static int saisirPoids() {
		System.out.println("poids en g. :");
		int poids = sc.nextInt();
		return poids;
	}
	public static int calculerTemps(int viande,int cuisson,int poids) {
		float coef =0.0f;
		if(viande == 1) { // BOEUF
			if(cuisson == 1) { //BLEU
				coef= 10.0f/500.0f;
			}
			else if(cuisson == 2) { //A POINT
				coef= 17.0f/500.0f;
			}
			else if(cuisson == 3) { //BIEN CUIT
				coef= 25.0f/500.0f;
			}

		}
		else if(viande == 2) { // PORC
			if(cuisson == 1) { //BLEU
				coef= 15.0f/400.0f;
			}
			else if(cuisson == 2) { //A POINT
				coef= 25.0f/400.0f;
			}
			else if(cuisson == 3) { //BIEN CUIT
				coef= 40.0f/400.0f;
			}

		}
		float tps = poids * coef * 60.0f; // *60 : en secondes
		int tps2 = (int) tps; 
		return tps2;
	}
	
}
