package controller;

public class MethodeController 
{

	public static void main(String[] args) 
	{
		float valeur = diviser(10,2);
		System.out.println(valeur);
		int num = multi(5,2);
		System.out.println(num);
		int x = compte("toto");
		System.out.println(x);
		String r = comparer("test","bonjour");
		System.out.println(r);
		String r2 = comparer("test","test");
		System.out.println(r2);
	}
	public static float diviser(int a, int b)
	{
		float z = a / b;
		return z;
	}
	public static int multi(int a,int b)
	{
		return a * b;
	}
	public static int compte(String str) {
		return str.length();
	}
	public static String comparer(String str1,String str2) {
		String valeur ="";
		if (str1.length() > str2.length()) {
			valeur = str1;
		}else if (str1.length() < str2.length()) {
			valeur=str2;
		}else {
			valeur="-1";
		}
		return valeur;
	}
}
