package controller;

public class DemoController {
	
	public static void main(String[] args) 
	{
		// appeler une m�thode qui ne retourne Rien
		String str = "Nicolas CAGE";
		afficherLigne();
		disBonjour("Brad PITT");
		afficherLigne();
		disBonjour(str);
		
		// m�tthode qui ne retourne RIEN et ne re�oit RIEN
		afficherLigne();
		
		// une m�thode qui ne re�oit rien ET retourne qlq chose
		int x = grandeQuestion();
		System.out.println(x);
	}
	
	public static void disBonjour(String nom) 
	{
		// afficher en console
		System.out.println("Bonjour " +nom);
	}
	
	public static void afficherLigne() {
		System.out.println("-------------");
		// PAS DE return
	}
	
	public static int grandeQuestion() 
	{
		int valeur = 42;
		return valeur;
	}
}
