package controller;

public class MainController 
{

	public static void main(String[] args) 
	{

		// un tableau de String OU chaine de car.
		String[] tableau = new String[3]; // taille : 3
		// indice 0, 1, et 2
		tableau[0] = "Hello";
		tableau[1] = "ola !";
		
		// afficher le tableau
		//for(int i=0;i<4;i++)
		for(int i=0;i<tableau.length;i++)
		{
			System.out.println(tableau[i]);
		}
		// afficher avec le for each
		for(String str:tableau)
		{
			System.out.println(str);
		}
		// effacer l'emplacement 0
		tableau[0]=null;

	}

}
