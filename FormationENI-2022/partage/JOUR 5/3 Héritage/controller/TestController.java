package controller;

import model.Cercle;
import model.Rectangle;

public class TestController {

	public static void main(String[] args) {
		Rectangle r1 = new Rectangle(22,0,40,10);
		System.out.println(r1.superficie());
		System.out.println(r1.getX());
		Cercle c1 = new Cercle(10,10,10);
		System.out.println(c1.superficie());
	}

}
