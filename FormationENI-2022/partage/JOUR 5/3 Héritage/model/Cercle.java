package model;

public class Cercle {
	private int x;
	private int y;
	private int rayon;
	public Cercle(int x, int y, int rayon) {
		super();
		this.x = x;
		this.y = y;
		this.rayon = rayon;
	}
	public float superficie() {
		return 3.14f *rayon * rayon;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getRayon() {
		return rayon;
	}
	public void setRayon(int rayon) {
		this.rayon = rayon;
	}
	@Override
	public String toString() {
		return "Cercle [x=" + x + ", y=" + y + ", rayon=" + rayon + "]";
	}
	
}
