package controller;

import model.Personne;

public class TestController {

	public static void main(String[] args) {
		String nom1 = "PITT";
		String prenom1="Brad";
		
		Personne p1 = new Personne("Brad","PITT");
		// p1 regroupe toute les variableas de Brad
		//System.out.println(p1.prenom); // PAS POSSIBLE prenom est "private"
		System.out.println(p1.getFullName());
		System.out.println(p1); // "appelle la m�thode to string"
		System.out.println(p1.toString()); // "appelle la m�thode to string"
	}

}
