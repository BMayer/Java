package model;

public class Personne {
	// varibles de classe : attributs ou propri�t�s
	private String prenom;
	private String nom;
	// le constructeur
	public Personne(String prenom, String nom) {
		this.prenom = prenom;
		this.nom = nom;
	}
	// methode interne
	// m�thoque qui affiche le "nom complet" : Brad PITT
	public String getFullName() {
		return prenom +" " +nom;
	}
	// getter et setter
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	// m�thode "to string"
	@Override
	public String toString() {
		return "Personne [prenom=" + prenom + ", nom=" + nom + "]";
	}
	
	
}
