package model;

public  class Rectangle extends Forme{
//	private int x;
//	private int y;
	private int longueur;
	private int largeur;
	
	public Rectangle(int x, int y, int longueur, int largeur) {
		// le "this" de la class mere
		// this -> super
		
		// appeler le constructeur de forme Forme(x,y)
		// ON NE FAIT f = new Forme()
		super(x,y);
		this.longueur = longueur;
		this.largeur = largeur;
	}
	// methode interne
	// je bloque l'ovveride ou la surcharge de la 
	// m�thode superficie dans la classe fille
	public int superficie() {
		return longueur *largeur;
	}
	// je ne veux pas que Carre redefinisse une m�thode superficie
	@Override
	public String decrisToi() {
		return super.decrisToi() + " je suis un rectangle";
	}
//	public int getX() {
//		return x;
//	}
//
//	public void setX(int x) {
//		this.x = x;
//	}
//
//	public int getY() {
//		return y;
//	}
//
//	public void setY(int y) {
//		this.y = y;
//	}

	public int getLongueur() {
		return longueur;
	}

	public void setLongueur(int longueur) {
		this.longueur = longueur;
	}

	public int getLargeur() {
		return largeur;
	}

	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}
	@Override
	public String toString() {
		return "Rectangle [longueur=" + getLongueur() + ", largeur=" + getLargeur() + ", x=" + super.getX() + ", y=" + super.getY() + "]";
	}


	
}
