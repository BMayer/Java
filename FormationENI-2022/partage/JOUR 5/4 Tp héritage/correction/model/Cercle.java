package model;

public class Cercle extends Forme{
	private int rayon;
	public Cercle(int x, int y, int rayon) {
		super(x,y);
		this.rayon = rayon;
	}
	public float superficie() {
		return 3.14f *rayon * rayon;
	}

	public int getRayon() {
		return rayon;
	}
	public void setRayon(int rayon) {
		this.rayon = rayon;
	}
	@Override
	public String toString() {
		return "Cercle test [x=" + getX() + ", y=" + getX() + ", rayon=" + rayon + "]";
	}
	
}
