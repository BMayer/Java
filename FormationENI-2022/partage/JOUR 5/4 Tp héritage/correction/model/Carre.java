package model;

public class Carre extends Rectangle {

	public Carre(int x, int y, int longueur, int largeur) {
		super(x, y, longueur, largeur);
	}

	@Override
	public String toString() {
		return "Carre [ x="+getX()+" y="+getY()+" cote=" + getLongueur() + "]";
	}

}
