package controller;

public class TableauController {

	public static void main(String[] args) 
	{
		int[] tableau = new int[4]; // taille 4;
		// Comment remplir le tableau
		tableau[0] = 56; // je met 56 � l'emplacemnt 0
		tableau[1] = 20; // je met 20 � l'emplacement 1
		tableau[3] = 24;
		// afficher le contenu d'un tableau
		//for(int i=0;i<4;i++)
		for(int i=0;i<tableau.length;i++) // i => 0 1 2 3
		{
			System.out.println(tableau[i]);
		}

	}

}
