package controller;

public class PersonneController {

	public static void main(String[] args) {

		// Cr�er un tableau de Chaine de caract. (String)
		// Pour stocker 3 personnes
		String[] tableau = new String[3];
		// Ajouter 3 personnes dans ce tableau
		// Afficher la liste des personnes
		tableau[0]= "Brad PITT";
		tableau[1]= "Bruce WILLIS";
		tableau[2]= "Nicolas CAGE";
		
		
		// Enlever une personne du tableau
		// Afficher la liste
		tableau[1] = null;
		// Trouver l'emplacment vide avec une boucle for
		// et rejouter une autre personne
		String invite = "Tom CRUISE";
		//tableau[1] = invite; //ON PEUT PAS
		for(int i=0;i<tableau.length;i++) 
		{
			if(tableau[i]==null) // si emplacement vide
			{
				tableau[i]=invite;
				break; // je sort de la bcl for
			}
		}
		// affichage
		for(String personne:tableau) 
		{
			System.out.println(personne);
		}
	}

}
