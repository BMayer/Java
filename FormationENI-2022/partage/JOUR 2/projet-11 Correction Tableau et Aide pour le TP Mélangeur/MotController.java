package controller;

import java.util.Random;

public class MotController {

	public static void main(String[] args) {
		// 1- Comment transformer un mot en tableau de caract�re
		String mot = "allo";
		char[] tabChar = mot.toCharArray(); 
		// afficher un tableau de caract�re
		System.out.println(tabChar);
		
		
		// 2- G�nerer un nombre al�atoire entre 0 et 9
		Random alea = new Random(); // je declare un objet de type Random
		int x = alea.nextInt(10);
		System.out.println(x);
		

		// 3- Comment transformer une phrase en tableau de mots
		// "coucou ca va"
		String phrase = "coucou ca va";
		String[] tabMot = phrase.split(" ");
		// tabMot[0]="coucou";
		// tabMot[1]="ca";
		// tabMot[2]="va";
		// ...
		for(String str : tabMot)
		{
			System.out.println(str);	
		}
	}

}


