package controler;

import model.Equipe;
import model.Personne;

public class TeamController {

	public static void main(String[] args) {
		Equipe e1 = new Equipe("Team A");
		Equipe e2 = new Equipe("Team B");
		Personne p1 = new Personne("Brad","PITT");
		Personne p2 = new Personne("Angelina","JOLIE");
		Personne p3 = new Personne("Nicolas","CAGE");
		Personne p4 = new Personne("Simone","VEIL");
		Personne p5 = new Personne("Tom","CRUISE");
		e1.ajouter(p1);
		e1.ajouter(p2);
		e2.ajouter(p3);
		e2.ajouter(p4);
		e2.ajouter(p5);
		System.out.println(e1);
		System.out.println(e2);
		e2.enlever(2);
		System.out.println(e2);
		

	}

}
