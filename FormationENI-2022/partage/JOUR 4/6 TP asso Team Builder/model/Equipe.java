package model;

public class Equipe {
	private String nom;
	private int indice=0;
	private Personne[] personnes = new Personne[4];

	public Equipe(String nom) {
		this.nom = nom;
	}
	public Equipe() {
	}

	public void ajouter(Personne p) {
		if (indice < 3) {
			personnes[indice] = p;
			this.indice++;
		}
	}
	public void enlever(int indice) {
		if((indice >= 0) &&(indice <4)) {
			personnes[indice] = null;
		}
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	public String afficher() {
		String str;
		str = "\n"+nom +"\n";
		for (Personne p:personnes) {
			if (p==null)
				break;
			str += " -"+p+"\n";
		}
		return str;
	}

	public String toString() {
		return afficher();
	}
}
