package model;

public class Ville {
	private String nom;
	private int nb;
	
	public Ville(String nom, int nb) {
		super();
		this.nom = nom;
		this.nb = nb;
	}
	public String comparer(Ville v) {
		if (v.getNb() > this.nb) {
			return " est plus petit que ";
		}
		else {
			return " est plus grand que ";
		}
	}
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getNb() {
		return nb;
	}

	public void setNb(int nb) {
		this.nb = nb;
	}

	@Override
	public String toString() {
		return "Ville [nom=" + nom + ", nb=" + nb + "]";
	}
	
}
