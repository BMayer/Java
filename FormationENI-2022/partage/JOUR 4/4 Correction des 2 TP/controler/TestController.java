package controler;

import model.Personne;
import model.Ville;

public class TestController {
	public static void main(String[] args) 
	{
		Personne p1 = new Personne("Brad","PITT");
		
		Personne p2 = new Personne();
		// hydratation
		// CTRL + ALT + DOWN
		p2.setNom("WILIS");
		p2.setPrenom("Bruce");
		Personne p3 = new Personne("Angelina","JOLIE");

		Ville v1 = new Ville("Brest",3);
		Ville v2 = new Ville("Nantes",2);
		String info = v1.comparer(v2);
		System.out.println(v1.getNom()+info+v2.getNom());
	}
}
