package model;

public class Rectangle {
	// zone des attributs ou propriétés
	//--------------------------------
	private int x;
	private int y;
//	public int toto;
	private int longueur;
	private int largeur;
	// source > Generate constructor
	public Rectangle(int x, int y, int longueur, int largeur) {
		this.x = x;
		this.y = y;
		this.longueur = longueur;
		this.largeur = largeur;
	}
	// source > getter ET setter
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getLongueur() {
		return longueur;
	}
	public void setLongueur(int longueur) {
		this.longueur = longueur;
	}
	public int getLargeur() {
		return largeur;
	}
	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}
	// source > generate to string
	@Override
	public String toString() {
		return "Rectangle [x=" + x + ", y=" + y + ", longueur=" + longueur + ", largeur=" + largeur + "]";
	}
	
	
}
