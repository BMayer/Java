package controller;

import model.Rectangle;

public class DessinController {

	public static void main(String[] args) {
		// cr�er un objet Rectangle
		Rectangle rectangle1 = new Rectangle(10,10,30,10);
		// tester private
		//System.out.println(rectangle1.x); NE MARCHE PAS !!!
		// System.out.println(rectangle1.toto);
		// rectangle1.toto = 42;
		// System.out.println(rectangle1.toto);
		System.out.println("---");
		// JE PEUX MODIFIER x
		System.out.println(rectangle1.toString());
		System.out.println(rectangle1.getX());
		rectangle1.setX(20);
		System.out.println(rectangle1.getX());
		System.out.println(rectangle1);
		
	}

}
