package model;

public class Cede {
	private String titre;
	private String chanteur;
	private Piste[] pistes;
	private int i;
	public Cede(String titre, String chanteur) {
		this.titre = titre;
		this.chanteur = chanteur;
		this.pistes = new Piste[20];
		i =0;
	}
	public void ajouter(Piste piste) {
 		pistes[i] =piste;
		this.i++;
	}
	public String toString() {
		String str = "titre :"+titre +" \n";
		str +="chanteur :"+chanteur+" \n";
		for (Piste piste: pistes) {
			if (piste == null) {
				break; // je sort de la bcl for each
			}
			str += "  -"+piste +"\n";
			//str += "  -"+piste.toString() +"\n";
		}
		return str;
	}
	
}
