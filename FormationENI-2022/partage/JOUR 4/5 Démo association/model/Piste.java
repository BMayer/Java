package model;

public class Piste {
	private int num;
	private String titre;
	
	public Piste(int num, String titre) {
		super();
		this.num = num;
		this.titre = titre;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	@Override
	public String toString() {
		return "Piste [num=" + num + ", titre=" + titre + "]";
	}
	
}
