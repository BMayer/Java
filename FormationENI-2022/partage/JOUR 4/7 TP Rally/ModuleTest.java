/**
 * 
 */
package fr.eni.rallye.test;

import java.util.GregorianCalendar;
import java.util.Scanner;

import fr.eni.rallye.model.Equipage;
import fr.eni.rallye.model.Equipe;


/**
 * @author bmartin
 *
 */
public class ModuleTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
        /*
         * définir les équipes
         */
        Equipe eqCitroen = new Equipe("Citroen", "FR", true);
        Equipage loeb = new Equipage(1, "LOEB", "Sebastien", "FR", "ELENA", "Daniel", "FR");
        eqCitroen.ajouterEquipage(loeb);
        eqCitroen.ajouterEquipage(new Equipage(2, "HIRVONEN", "Mikko", "FL", "LEHTINEN", "Jarma", "FL"));
        Equipe eqFord = new Equipe("Ford", "USA", true);
        eqFord.ajouterEquipage(new Equipage(3, "PILOTE", "Pilote", "USA", "COPILOTE", "Copilote", "USA"));

        System.out.println("**Composition de l'équipe Citroen");
        System.out.println("*********************************");
        System.out.println(eqCitroen.infosEquipe());
        System.out.println("**Composition de l'équipe Ford");
        System.out.println("******************************");
        System.out.println(eqFord.infosEquipe());

        
		System.out.println("Appuyez sur entrée pour sortir du test...");
		Scanner sc = new Scanner(System.in);
		sc.nextLine();
	
	}

}
