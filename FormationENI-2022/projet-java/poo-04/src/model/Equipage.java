package model;

public class Equipage {
	private Concurrent pilote;
	private Concurrent copilote;
	private int dossard;
	
	public String infosEquipage() {
		return this.toString();
	}

	// ---
	public Equipage(Concurrent pilote, Concurrent copilote, int dossard) {
		super();
		this.pilote = pilote;
		this.copilote = copilote;
		this.dossard = dossard;
	}

	public int getDossard() {
		return this.dossard;
	}

	@Override
	public String toString() {
		return "Equipage [pilote=" + pilote + ", copilote=" + copilote + ", dossard=" + dossard + "]";
	}

}
