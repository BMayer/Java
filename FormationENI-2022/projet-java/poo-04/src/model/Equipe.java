package model;

public class Equipe {
	private String nom;
	private String nationalite;
	private boolean constructeur;
	private int equipageIndex = 0;
	private Equipage[] equipages;
	
	// TODO ajouterEquipage(Equipage equipage)
	// equipageIndex++
	
	
	// TODO getInfosEquipe()
	
	// TODO getEquipage(int dossard)
	/*
	 * Equipage e = null
	 * loop equipes -> Equipage eq
	 * 		if eq.getDossard() == dossard
	 * 			e = eq
	 * 			break
	 * return e 
	 */
	
	// ---
	public Equipe() {
		super();
	}

	public Equipe(String nom, String nationalite, boolean constructeur) {
		super();
		this.nom = nom;
		this.nationalite = nationalite;
		this.constructeur = constructeur;
		this.equipages = new Equipage[4];
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getNationalite() {
		return nationalite;
	}

	public void setNationalite(String nationalite) {
		this.nationalite = nationalite;
	}

	public boolean isConstructeur() {
		return constructeur;
	}

	public void setConstructeur(boolean constructeur) {
		this.constructeur = constructeur;
	}

	@Override
	public String toString() {
		return "Equipe [nom=" + nom + ", nationalite=" + nationalite + ", constructeur=" + constructeur + "]";
	}
	
	

}
