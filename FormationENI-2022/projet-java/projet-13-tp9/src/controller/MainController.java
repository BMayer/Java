package controller;

public class MainController {

	public static void main(String[] args) {
		// diviser 2 nombres
		System.out.println("Diviser 8 / 2 [" + diviser(8, 2) + "]");
		System.out.println("Diviser 3.14 / 27.42 [" + diviser(3.14, 27.42) + "]");
		System.out.println("Diviser 7 / 0 [" + diviser(7, 0) + "]");
		System.out.println("Diviser 0 / 7 [" + diviser(0, 7) + "]");
		// multiplier 2 nombres
		System.out.println("Multiplier 8 * 2 [" + multiplier(8, 2) + "]");
		System.out.println("Multiplier 8 * 0 [" + multiplier(8, 2) + "]");
		System.out.println("Multiplier 8 * MAX_VALUE [" + multiplier(8, Double.MAX_VALUE) + "]");
		// retourne le nbr de car. d'1 mot
		String mot = "anticonstitutionnellement";
		System.out.println("Longeur '" + mot + "' [" + longueurMot(mot) + "]");
		mot = "";
		System.out.println("Longeur '" + mot + "' [" + longueurMot(mot) + "]");
		mot = null;
		System.out.println("Longeur '" + mot + "' [" + longueurMot(mot) + "]");
		// De 2 mots recus, retourne le + long. si == alors String "-1"
		String mot1 = "court";
		String mot2 = "anticonstitutionnellement";
		System.out.println("Compare '" + mot1 + "' et '" + mot2 + "' [" + compare2Mots(mot1, mot2) + "]");
		mot2 = null;
		System.out.println("Compare '" + mot1 + "' et '" + mot2 + "' [" + compare2Mots(mot1, mot2) + "]");
	}
	
	public static double diviser(double dividende, double diviseur) {
		/*if (diviseur == 0) {
			return Double.NaN;
		}*/
		return dividende / diviseur;
	}
	
	public static double multiplier(double facteur1, double facteur2) {
		return facteur1 * facteur2;
	}
	
	public static int longueurMot(String mot) {
		if (mot != null) {
			return mot.length();
		}
		else {
			return -1;
		}
	}
	
	public static String compare2Mots(String mot1, String mot2) {
		if (mot1 == null || mot2 == null) {
			return null;
		}
		int lenMot1 = mot1.length();
		int lenMot2 = mot2.length();
		if (lenMot1 == lenMot2) {
			return "-1";
		}
		else if (lenMot1 > lenMot2) {
			return mot1;
		}
		else {
			return mot2;
		}
	}

}
