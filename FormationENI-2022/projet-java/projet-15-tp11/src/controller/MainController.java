package controller;

import java.util.Scanner;

public class MainController {

	public static void main(String[] args) {
		// Afficher un menu
		// Recevoir action et l'appeler
		// methode "Ajouter une personne"
		// methode "Affichier liste de personne"
		int retCode = 0;
		String[] listePersonnes = new String[10];
		Scanner sc = new Scanner(System.in);
		int caract = 0;
		
		do {
			afficheMenu();
			caract = sc.nextInt();
			if (caract == 0) {
				// Ciao !
				System.out.println("A une prochaine fois !");
				break;
			}
			else if (caract == 1) {
				retCode = afficheListePersonnes(listePersonnes);
			}
			else if (caract == 2) {
				retCode = ajoutePersonneALaListe(listePersonnes, sc);
				if (retCode != 0) {
					// Liste pleine
					System.out.println("La liste est pleine !");
				}
			}
			else {
				System.out.println("Je ne comprend pas [" + caract + "]...");
				caract = -1;
			}
		} while (caract != 0);
		sc.close();
	}
	
	public static int ajoutePersonneALaListe(String[] listePersonnes, Scanner sc) {
		// Portee de la String[] ?
		System.out.println("Quelle personne ?");
		String personne = sc.next();
		for (int i = 0; i < listePersonnes.length; i++) {
			if (listePersonnes[i] == null  ) { //.equals("\0")
				listePersonnes[i] = personne;
				System.out.println("[" + personne + "] i:[" + i + "]");
				return 0;
			}
		}
		return 1;
	}
	
	public static int afficheListePersonnes(String[] listePersonnes) {
		// Le liste peut etre vide ...
		for (int i = 0; i < listePersonnes.length; i++) {
			// La personne peut etre NULL
			System.out.println("* " + i + "\t" + listePersonnes[i]);
		}
		return 0;
	}

	public static void afficheMenu() {
		System.out.println("************************************");
		System.out.println("*  1 - Afficher liste de personnes *");
		System.out.println("*  2 - Ajouter une personne        *");
		System.out.println("*                                  *");
		System.out.println("*                                  *");
		System.out.println("*  0 - Quitter                     *");
		System.out.println("*                                  *");
		System.out.println("************************************");		
	}
}
