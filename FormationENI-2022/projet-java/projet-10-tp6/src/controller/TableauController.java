package controller;

public class TableauController {

	public static void main(String[] args) {
		// for (int i=0;i<4;i++) {...}
		// tableau int[] tableau = new int[4] // par defaut, la cellule est init a 0
		int borne = 0;
		
		// 
		borne = 5;
		int[] aSuite = new int[borne];
		for (int i = 0; i < borne; i++) {
			aSuite[i] = i;
			System.out.println("Suite i: [" + i + "] : " + aSuite[i]);
		}
		
		System.out.println("--------------------------");
		
		//
		borne = 11;
		int[] aDecuple = new int[borne];
		for (int i = 0; i < borne; i++ ) {
			aDecuple[i] = i * 10;
			System.out.println(" Decuple i: [" + i + "] : " + aDecuple[i]);
		}
		
		System.out.println("--------------------------");
		
		//
		borne = 11;
		int[] aDouble = new int[borne];
		for (int i = 0; i < borne; i++ ) {
			aDouble[i] = i * 2;
			System.out.println(" Double i: [" + i + "] : " + aDouble[i]);
		}		

	}

}
