package model;

import java.util.Arrays;

public class Rectangle {
	// Attributs (ou variables de classe)
	private int x;
	private int y;
	public int toto;
	private int longueur;
	private int largeur;
	private int[] liste;
	
	public Rectangle(int x, int y, int longueur, int largeur) {
		// this permet d'appeler les attributs ? ? ?
		this.x = x;
		this.y = y;
		this.longueur = longueur;
		this.largeur = largeur;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getToto() {
		return toto;
	}

	public void setToto(int toto) {
		this.toto = toto;
	}

	public int getLongueur() {
		return longueur;
	}

	public void setLongueur(int longueur) {
		this.longueur = longueur;
	}

	public int getLargeur() {
		return largeur;
	}

	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}

	@Override
	public String toString() {
		return "Rectangle [x=" + x + ", y=" + y + ", toto=" + toto + ", longueur=" + longueur + ", largeur=" + largeur
				+ ", liste=" + Arrays.toString(liste) + "]";
	}

	public int[] getListe() {
		return liste;
	}

	public void setListe(int[] liste) {
		this.liste = liste;
	}
	
	
}
