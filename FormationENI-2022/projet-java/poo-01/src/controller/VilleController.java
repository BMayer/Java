package controller;

import model.Ville;

public class VilleController {

	public static void main(String[] args) {

		Ville grosseVille = new Ville("Brest", 150000);
		Ville petiteVille = new Ville("Ploermel", 3500);
		Ville village = new Ville("Josselin", 1000);
		
		if (petiteVille.isPlusHabitantsQue(grosseVille)) {
			System.out.println(grosseVille.getNom() + " est plus grosse que " + petiteVille.getNom());
		}
		else {
			System.out.println(grosseVille.getNom() + " n'est pas plus grosse que " + petiteVille.getNom());
		}

		if (petiteVille.isPlusHabitantsQue(village)) {
			System.out.println(village.getNom() + " est plus grosse que " + petiteVille.getNom());
		}
		else {
			System.out.println(village.getNom() + " n'est pas plus grosse que " + petiteVille.getNom());
		}
		
	}

}
