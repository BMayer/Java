package controller;

import model.Personne;

public class PersonController {

	public static void main(String[] args) {
		// Creer 3 personnes
		
		Personne person1 = new Personne("Simone", "Veil");
		Personne person2 = new Personne("Gisele", "Halimi");
		Personne person3 = new Personne("Greta", "Thunberg");
		
		System.out.println("Les 3 personnes sont :");
		System.out.println(" - " + person1.toString());
		System.out.println(" - " + person2.toString());
		System.out.println(" - " + person3.toString());

	}

}
