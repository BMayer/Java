package controller;

public class TestController {

	public static void main(String[] args) {
		// Tester si nombre positif ou negatif
		int x = 15;
		if (x > 0) {
			System.out.println(x + " est positif");
		}
		else if (x == 0) {
				System.out.println(" 0 est positif ET negatif");
		}
		else {
			System.out.println(x + " est negatif");
		}

	}

}
