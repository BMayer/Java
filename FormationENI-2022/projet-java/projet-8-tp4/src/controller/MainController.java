package controller;

import java.util.Scanner;

public class MainController {

	public static void main(String[] args) {
		int poids = 0;
		int matiere = 0; // 1 boeuf, 2 porc
		int cuisson = 0; // 1 bleu, 2 a point, 3 bien cuit
		int coef = 0;
		int BOEUF = 1;
		int PORC = 2;
		int BLEUE = 1;
		int A_POINT = 2;
		int BIEN_CUIT = 3;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Quel poids (en gr) ? : ");
		poids = sc.nextInt();
		if (poids <= 0) {
			System.out.println("Pb avec le poids [" + poids + "]");
			System.exit(1);
		}
		
		System.out.println("Quelle matiere (1 boeuf | 2 porc) ?");
		matiere = sc.nextInt();
		if (matiere != 1 && matiere != 2) {
			System.out.println("Pb avec la matiere, 1 pour le boeuf ou bien 2 pour le porc ");
			System.exit(2);
		}
		
		System.out.println("Quelle cuisson (1 bleu | 2 a point | 3 bien cuit) ?");
		cuisson = sc.nextInt();
		if (cuisson < 1 || cuisson > 3) {
			System.out.println("Pb avec la cuisson (bla bla bla)");
			System.exit(3);
		}

		if (matiere == BOEUF) {
			if (cuisson == BLEUE) {
				// 20'/kg
				coef = (20 * 60);

			}
			else if (cuisson == A_POINT) {
				// a point 34'/kg
				coef = (34 * 60);				
			}
			else if (cuisson == BIEN_CUIT) {
				// bien cuit 50'/kg
				coef = (50 * 60);
			}
			else {
				System.out.println("Cuisson de " + BOEUF + " inconnue");
			}
		}
		else if (matiere == PORC) {
			if (cuisson == BLEUE) {
				// bleu 15 / 0.4
				coef = (int) ((15 / 0.4) * 60);			
			}
			else if (cuisson == A_POINT) {
				// a point  25 / 0.4
				coef = (int) ((25 / 0.4) * 60);			
			}
			else if (cuisson == BIEN_CUIT) {
				// bien cuit 40 / 0.4
				coef = (int) ((40 / 0.4) * 60);		
			}
			else {
				System.out.println("coef porc [" + coef + "]");
				System.out.println("Cuisson de " + PORC + " inconnue");
			}
			
		}
		else {
			System.out.println("On ne devrait pas etre dans cette matiere inconnue");
		}
		
		System.out.println("Tps cuisson : "
				+ (int) (coef * poids) / 1000
				+ " secondes");
		
		sc.close();
		System.exit(0);
	}

}
