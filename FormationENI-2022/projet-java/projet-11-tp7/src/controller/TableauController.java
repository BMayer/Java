package controller;
/*
	TP7 Liste de Personnes
	
	Cr�er un tableau de Chaine de caract. (String)
	Pour stocker 3 personnes
	
	Ajouter 3 personnes dans ce tableau
	Afficher la liste des personnes
	
	Enlever une personne du tableau
	Afficher la liste
	
	Trouver l'emplacment vide avec une boucle for 
	et rejouter une autre personne
 */
public class TableauController {
	String personne1 = "p1";
	String personne2 = "p2";
	String personne3 = "p3";
	
	/*
	 * Boucle       FOR   EACH
	 * for (variable : tableau) { }
	 */
	
	public static void main(String[] args) {
		int nbrCells = 3;
		String[] aPersonnes = new String[nbrCells];
		aPersonnes[0] = "p1";
		aPersonnes[1] = "p2";
		aPersonnes[2] = "p3";
		
		for (int i = 0; i < aPersonnes.length; i++) {
			System.out.println("aPersonnes : [" + i + "] = " + aPersonnes[i]);
		}
		
		aPersonnes[1] = null;
		
		System.out.println("-------------------------------------------");
		for (int i = 0; i < aPersonnes.length; i++) {
			if (aPersonnes[i] != null) {
				System.out.println("aPersonnes : [" + i + "] = " + aPersonnes[i]);
			}
			else {
				System.out.println("Il n'existe pas de personne definie pour l'indice " + i);
			}
		}

	}

}
