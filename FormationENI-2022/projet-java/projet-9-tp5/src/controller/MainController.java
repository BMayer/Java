package controller;

import java.util.Scanner;

public class MainController {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int numeroChq = -1;
		float montant = 0;
		int nombreChq = 0;
		float montantTotal = 0;
		float moyenneMontant = 0;
		int nombreChqInfSeuil = 0;
		float montantChqInfSeuil = Float.MAX_VALUE;
		int numeroChqInfSeuil = 0;
		float montantTotalInfSeuil = 0.0f;
		int nombreChqSupSeuil = 0;
		float montantChqSupSeuil = 0;
		int numeroChqSupSeuil = 0;
		float montantTotalSupSeuil = 0.0f;
		
		float SEUIL = 200.00f;

		do {
			System.out.println("numero de chq ?");
			numeroChq = sc.nextInt();
			System.out.println("montant ?");
			montant = sc.nextFloat();
			
			if (numeroChq != 0 && montant > 0) {
				nombreChq++;
				montantTotal += montant;
				
				if (montant < SEUIL) {
					nombreChqInfSeuil++;
					montantTotalInfSeuil += montant;
					if (montant < montantChqInfSeuil) {
						numeroChqInfSeuil = numeroChq;
						montantChqInfSeuil = montant;
					}
				}
				else {
					nombreChqSupSeuil++;
					montantTotalSupSeuil += montant;
					if (montant >= montantChqSupSeuil) {
						numeroChqSupSeuil = numeroChq;
						montantChqSupSeuil = montant;
					}
				}
			}		
			// Si Float MAX inexistant
			/*if (nombreChq == 1) {
				numeroChqInfSeuil = numeroChqSupSeuil = numeroChq;
				montantChqInfSeuil = montantChqSupSeuil = montant;
			}*/
		} while (numeroChq != 0);
		moyenneMontant = montantTotal / nombreChq;

		// Global
		System.out.println("Global");
		System.out.println("Nbr chq [" + nombreChq + "] pour montant [" + montantTotal + "]");
		System.out.println("Soit chq moyen [" + moyenneMontant + "]");
		System.out.println("");
		
		// INF
		System.out.println("Num chq INF [" + numeroChqInfSeuil + "] pour montant [" + montantChqInfSeuil + "]");
		System.out.println("Nbr chq INF [" + nombreChqInfSeuil + "] pour montant [" + montantTotalInfSeuil + "]");
		System.out.println("");
		
		// SUP
		System.out.println("Num chq SUP [" + numeroChqSupSeuil + "] pour montant [" + montantChqSupSeuil + "]");
		System.out.println("Nbr chq SUP [" + nombreChqSupSeuil + "] pour montant [" + montantTotalSupSeuil + "]");
		System.out.println("");
		
		sc.close();
		System.exit(0);

	}

}