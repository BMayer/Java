package controller;

import java.util.Scanner;

public class CuissonController {

	/* Le scanner devrait etre declare ici, afin d'etre vu de toutes les methodes */
	// private static Scanner sc;
	/* Cela eviterait de passer sc en parametre des methodes. */
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		//System.out.println("Viande : 1 Boeuf - 2 Porc");
		//int viande = sc.nextInt();
		int viande = saisirTypeViande(sc);
		//System.out.println("Cuisson : 1 BLEU - 2 A POINT - 3 BIEN CUIT");
		//int cuisson = sc.nextInt();
		int cuisson = saisirTypeCuisson(sc);
		//System.out.println("poids en g. :");
		//int poids = sc.nextInt();
		int poids = saisirPoids(sc);
		
		/*float coef =0.0f;
		if(viande == 1) { // BOEUF
			if(cuisson == 1) { //BLEU
				coef= 10.0f/500.0f;
			}
			else if(cuisson == 2) { //A POINT
				coef= 17.0f/500.0f;
			}
			else if(cuisson == 3) { //BIEN CUIT
				coef= 25.0f/500.0f;
			}

		}
		else if(viande == 2) { // PORC
			if(cuisson == 1) { //BLEU
				coef= 15.0f/400.0f;
			}
			else if(cuisson == 2) { //A POINT
				coef= 25.0f/400.0f;
			}
			else if(cuisson == 3) { //BIEN CUIT
				coef= 40.0f/400.0f;
			}

		}
		float tps = poids * coef * 60.0f; // *60 : en secondes
		int tps2 = (int) tps; */
		
		int tps2 = calculerTemps(viande, cuisson, poids);
		System.out.println("Votre temps de cuisson :" 
				+ tps2 
				+ " secondes");

		sc.close();
		
	}
	
	public static int saisirTypeViande(Scanner sc) {
		System.out.println("Viande : 1 Boeuf - 2 Porc");
		return sc.nextInt();
	}
	
	public static int saisirTypeCuisson(Scanner sc) {
		System.out.println("Cuisson : 1 BLEU - 2 A POINT - 3 BIEN CUIT");
		return sc.nextInt();
	}
	
	public static int saisirPoids(Scanner sc) {
		System.out.println("poids en g. :");
		return sc.nextInt();
	}
	
	public static int calculerTemps(int viande, int cuisson, int poids) {
		float coef =0.0f;
		if(viande == 1) { // BOEUF
			if(cuisson == 1) { //BLEU
				coef= 10.0f / 500.0f;
			}
			else if(cuisson == 2) { //A POINT
				coef= 17.0f / 500.0f;
			}
			else if(cuisson == 3) { //BIEN CUIT
				coef= 25.0f / 500.0f;
			}
		}
		else if(viande == 2) { // PORC
			if(cuisson == 1) { //BLEU
				coef= 15.0f / 400.0f;
			}
			else if(cuisson == 2) { //A POINT
				coef= 25.0f / 400.0f;
			}
			else if(cuisson == 3) { //BIEN CUIT
				coef= 40.0f / 400.0f;
			}
		}
		float tps = poids * coef * 60.0f; // *60 : en secondes
		
		return (int) tps;
	}

}
