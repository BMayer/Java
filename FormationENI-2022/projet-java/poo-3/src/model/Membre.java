package model;

public class Membre {
	
	private String prenom;
	private String nom;
	
	public Membre() {
		super();
	}

	public Membre(String prenom, String nom) {
		super();
		this.prenom = prenom;
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "Membre [prenom=" + prenom + ", nom=" + nom + "]";
	}

}
