package model;

import java.util.Arrays;

public class Team {

	//private Membre membre;
	private String nomEquipe;
	private Membre[] membres = new Membre[5];
	
	public Team() {
		super();
	}
	
	public Team(String nomEquipe) {
		super();
		this.nomEquipe = nomEquipe;
	}

	public boolean delMembre(int i) {
		if (i < 0 || i > membres.length || membres[i] == null) {
			return false;
		}
		else {
			membres[i] = null;
			return true;
		}
	}

	public String getMembre(int i) {
		return membres[i].toString();
	}

	public boolean setMembre(Membre nouveauMembre) {
		for (int i = 0; i < this.membres.length; i++) {
			if (this.membres[i] == null) {
				this.membres[i] = nouveauMembre;
				return true;
			}
		}
		return false;
	}

	public String getNomEquipe() {
		return nomEquipe;
	}

	public void setNomEquipe(String nomEquipe) {
		this.nomEquipe = nomEquipe;
	}

	private String genereToString() {
		String retCode = "Team [" + this.nomEquipe + "]\n";
		for (int i = 0; i  < membres.length; i++) {
			if (membres[i] != null) {
				retCode += " - [\t" + i + "] [" + membres[i].getPrenom() + ", \t" + membres[i].getNom() + "]\n";
			}
			else {
				retCode += " - [\t" + i + "] - absent\n";
			}
		}
		return retCode;
	}
	@Override
	public String toString() {
		//return "Team [nomEquipe=" + nomEquipe + ", membres=" + Arrays.toString(membres) + "]";
		return this.genereToString();
	}

	
}
