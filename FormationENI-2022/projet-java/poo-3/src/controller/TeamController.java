package controller;

import model.Membre;
import model.Team;

public class TeamController {

	public static void main(String[] args) {
		// Monter une equipe A avec 2 membres
		Membre membreAA = new Membre("Angelina", "Jolie");
		Membre membreAB = new Membre();
		membreAB.setPrenom("Agriculture");
		membreAB.setNom("Bio");
		
		Team teamA = new Team();
		teamA.setNomEquipe("Team A");
		teamA.setMembre(membreAA);
		teamA.setMembre(membreAB);
		
		// Indesirable dans equipe
		teamA.setMembre(new Membre("Tom", "Cruise"));
		
		// Monter une equipe B avec 2 membres
		Team teamB = new Team("Team B");
		teamB.setMembre(new Membre("Nicolas", "Cage"));
		teamB.setMembre(new Membre("Simone", "Veil"));
		
		// Trop de membres ,
		Team teamC = new Team("Team C");
		for (int i = 65; i < 72; i++) {
			String nom = Character.toString((char) i);
			String prenom = Character.toString((char) (i + 32));
			boolean retCode = teamC.setMembre(new Membre(prenom, nom));
			if (retCode) {
				System.out.println("Ok pour " + prenom + " " + nom);
			}
			else {
				System.out.println("! KO pour " + prenom + " " + nom);
			}
		}
		
		System.out.println("teamA : \n" + teamA.toString());
		System.out.println("Del de Tom Cruise");
		teamA.delMembre(2);
		System.out.println("teamA : \n" + teamA.toString());
		
	}

}
