package model;

public class Titre {
	
	private int numPiste;
	private String morceau;
	
	public Titre() {
		super();
	}

	public Titre(int numPiste, String morceau) {
		super();
		this.numPiste = numPiste;
		this.morceau = morceau;
	}

	public int getNumPiste() {
		return numPiste;
	}

	public void setNumPiste(int numPiste) {
		this.numPiste = numPiste;
	}

	public String getMorceau() {
		return morceau;
	}

	public void setMorceau(String morceau) {
		this.morceau = morceau;
	}
	
	

}
