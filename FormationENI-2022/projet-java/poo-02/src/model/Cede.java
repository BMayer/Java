package model;

public class Cede {

	private String album;
	private String artiste;
	private Titre[] titres;
	private int pisteActive = 0;
	
	public Cede() {
		super();
	}
	
	public Cede(String album, String artiste) {
		super();
		this.album = album;
		this.artiste = artiste;
	}

	public String getAlbum() {
		return album;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	public String getArtiste() {
		return artiste;
	}

	public void setArtiste(String artiste) {
		this.artiste = artiste;
	}

	public Titre[] getTitres() {
		return titres;
	}

	public void setTitres(Titre[] titres) {
		this.titres = titres;
	}

	public int getPisteActive() {
		return pisteActive;
	}

	public void setPisteActive(int pisteActive) {
		this.pisteActive = pisteActive;
	}
	
	
}
