package model;

public class Capitale extends Ville {
	String monument;

	
	public Capitale(String nom, int nbrHabitants) {
		super(nom, nbrHabitants);
	}


	public Capitale(String nom, int nbrHabitants, String monument) {
		super(nom, nbrHabitants);
		this.monument = monument;
	}


	public String getMonument() {
		return monument;
	}


	public void setMonument(String monument) {
		this.monument = monument;
	}


	@Override
	public String toString() {
		return "Capitale [monument=" + monument + "] parent : " + super.toString();
	}
	
	

}
