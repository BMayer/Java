package model;

public class Ville {
	private String nom;
	private int nbrHabitants;
	public Ville(String nom, int nbrHabitants) {
		super();
		this.nom = nom;
		this.nbrHabitants = nbrHabitants;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getNbrHabitants() {
		return nbrHabitants;
	}
	public void setNbrHabitants(int nbrHabitants) {
		this.nbrHabitants = nbrHabitants;
	}
	@Override
	public String toString() {
		return "Ville [nom=" + nom + ", nbrHabitants=" + nbrHabitants + "]";
	}
	
	
}
