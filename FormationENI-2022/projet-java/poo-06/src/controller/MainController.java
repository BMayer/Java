package controller;

import model.Capitale;
import model.Ville;

public class MainController {

	public static void main(String[] args) {
		Ville v = new Ville("Phalsbourg", 7890);
		System.out.println("ville " + v.toString());

		Capitale c = new Capitale("Strasbourg", 123456, "place Kleber");
		System.out.println("capitale " + c.toString());
	}

}
