package model;

public abstract class Personne {
	protected String siteWeb;


	public Personne() {
		super();
	}
	
	public Personne(String siteWeb) {
		super();
		this.siteWeb = siteWeb;
	}

	public String getSiteWeb() {
		return siteWeb;
	}

	public void setSiteWeb(String siteWeb) {
		this.siteWeb = siteWeb;
	}

	@Override
	public String toString() {
		return "Personne [siteWeb=" + siteWeb + "]";
	}
	
	
}
