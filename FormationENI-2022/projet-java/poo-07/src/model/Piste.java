package model;

public class Piste {
	private int piste;
	private String intitule;
	private int duree;
	
	public Piste(int piste, String intitule, int duree) {
		super();
		this.piste = piste;
		this.intitule = intitule;
		this.duree = duree;
	}
	
	public int getPiste() {
		return piste;
	}
	public void setPiste(int piste) {
		this.piste = piste;
	}
	public String getIntitule() {
		return intitule;
	}
	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}
	public int getDuree() {
		return duree;
	}
	public void setDuree(int duree) {
		this.duree = duree;
	}
	
	@Override
	public String toString() {
		return "Piste [piste=" + piste + ", intitule=" + intitule + ", duree=" + duree + "]";
	}
}
