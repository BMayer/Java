package model;

public class Editeur extends Personne {
	private String nomGroupe;
	private String adresse;
	
	public Editeur(String nomGroupe, String adresse) {
		super();
		this.nomGroupe = nomGroupe;
		this.adresse = adresse;
	}
	public Editeur(String nomGroupe, String adresse, String siteWeb) {
		super(siteWeb);
		this.nomGroupe = nomGroupe;
		this.adresse = adresse;
	}
	public String getNomGroupe() {
		return nomGroupe;
	}
	public void setNomGroupe(String nomGroupe) {
		this.nomGroupe = nomGroupe;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	
	@Override
	public String toString() {
		return "Editeur [nomGroupe=" + nomGroupe + ", adresse=" + adresse + "]";
	}
	
	
}
