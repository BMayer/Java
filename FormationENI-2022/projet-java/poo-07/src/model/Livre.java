package model;

import java.util.Date;

public class Livre extends Produit {

	public Livre(String titre, Date dateSortie, float prixAchat, String auteurNom, String auteurPrenom) {
		super(titre, dateSortie, prixAchat, auteurNom, auteurPrenom);
	}

	@Override
	public String toString() {
		return "Livre []";
	}
	
	
}
