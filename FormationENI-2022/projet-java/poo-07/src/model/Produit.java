package model;

import java.util.Date;

public abstract class Produit {
	private String titre;
	private Date   dateSortie;
	private float  prixAchat;
	private String auteurNom;
	private String auteurPrenom;
	Auteur auteur = new Auteur(auteurNom, auteurPrenom);
	
	public Produit(String titre, Date dateSortie, float prixAchat, String auteurNom, String auteurPrenom) {
		super();
		this.titre = titre;
		this.dateSortie = dateSortie;
		this.prixAchat = prixAchat;
		this.auteurNom = auteurNom;
		this.auteurPrenom = auteurPrenom;
	}
	
	
	float getPrixVente() {
		return this.prixAchat;
	}
	float getPrixVente(float reduction) {
		return this.prixAchat * reduction;
	}


	public String getTitre() {
		return titre;
	}


	public void setTitre(String titre) {
		this.titre = titre;
	}


	public Date getDateSortie() {
		return dateSortie;
	}


	public void setDateSortie(Date dateSortie) {
		this.dateSortie = dateSortie;
	}


	public float getPrixAchat() {
		return prixAchat;
	}


	public void setPrixAchat(float prixAchat) {
		this.prixAchat = prixAchat;
	}


	public String getAuteurNom() {
		return auteurNom;
	}


	public void setAuteurNom(String auteurNom) {
		this.auteurNom = auteurNom;
	}


	public String getAuteurPrenom() {
		return auteurPrenom;
	}


	public void setAuteurPrenom(String auteurPrenom) {
		this.auteurPrenom = auteurPrenom;
	}


	public Auteur getAuteur() {
		return auteur;
	}


	public void setAuteur(Auteur auteur) {
		this.auteur = auteur;
	}


	@Override
	public String toString() {
		return "Produit [titre=" + titre + ", dateSortie=" + dateSortie + ", prixAchat=" + prixAchat + ", auteurNom="
				+ auteurNom + ", auteurPrenom=" + auteurPrenom + ", auteur=" + auteur + "]";
	}
	
	
}
