package controller;

import model.Carre;
import model.Cercle;
import model.Rectangle;

public class MainDessin {

	public static void main(String[] args) {
		Rectangle rectangle1 = new Rectangle(0, 0, 123, 68);
		System.out.println("r1 3, 4, 123, 68 " + rectangle1.toString());
		System.out.println("superficie : " + rectangle1.getSuperficie());
		
		Rectangle rectangle2 = new Rectangle(147, 254);
		System.out.println("r2 147, 254 " + rectangle2.toString());
		System.out.println("superficie : " + rectangle2.getSuperficie());
		
		Cercle cercle1 = new Cercle(8, 9, 258);
		System.out.println("c1 " + cercle1.toString());
		System.out.println("superficie : " + cercle1.getSuperficie());
		
		Carre carre1 = new Carre(57, 68, 79);
		System.out.println("k1 " + carre1.toString());
		System.out.println("superficie : " + carre1.getSuperficie());
		
	}

}
