package model;

/**
 * @author Administrateur
 *
 */
public class Cercle extends Forme {
	private int rayon = 50;
	//private int x = 0;
	//private int y = 0;
	//Forme forme = new Forme();
	public Cercle() {
		super();
	}
	public Cercle(int x, int y) {
		super(x, y);
//		this.x = x;
//		this.y = y;
	}
	public Cercle(int x, int y, int rayon) {
		super(x, y);
//		this.x = x;
//		this.y = y;
		this.rayon = rayon;
	}
	public int getRayon() {
		return rayon;
	}
	public void setRayon(int rayon) {
		this.rayon = rayon;
	}
@Override
	public String toString() {
		return "Cercle [rayon=" + rayon + "] parent : " + super.toString();
	}

	public int getSuperficie() {
		return (int) 3.14 * this.rayon * this.rayon;
	}
	
}
