package model;

public class Rectangle extends Forme {
	private int longueur = 100;
	private int largeur = 67;
	//private int x = 0;
	//private int y = 0;
	//private Forme forme = new Forme();
	public Rectangle(int x, int y, int longueur, int largeur) {
		super(x, y);
//		this.x = x;
//		this.y = y;
		this.longueur = longueur;
		this.largeur = largeur;
//		forme.setX(this.x);
//		forme.setY(this.y);
	}
	public Rectangle(int x, int y) {
		super(x, y);
//		this.x = x;
//		this.y = y;
//		forme.setX(this.x);
//		forme.setY(this.y);
	}
	public Rectangle() {
		super();
	}
	public int getLongueur() {
		return longueur;
	}
	public void setLongueur(int longueur) {
		this.longueur = longueur;
	}
	public int getLargeur() {
		return largeur;
	}
	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}
	@Override
	public String toString() {
		return "Rectangle [longueur=" + longueur + ", largeur=" + largeur + "] parent " + super.toString();
	}

	public int getSuperficie() {
		return this.largeur * this.longueur;
	}
	

}
