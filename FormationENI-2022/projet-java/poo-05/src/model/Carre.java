package model;

public class Carre extends Rectangle {
	int cote = 88;
	
	public Carre(int x, int y, int cote) {
		super(x, y, cote, cote);
		this.cote = cote;
	}

	public int getCote() {
		return cote;
	}

	public void setCote(int cote) {
		this.cote = cote;
	}

	@Override
	public String toString() {
		return "Carre [cote=" + cote + "] parent " + super.toString();
	}


}
