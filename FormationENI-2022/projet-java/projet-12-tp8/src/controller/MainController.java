package controller;

import java.util.Random;

public class MainController {

	public static void main(String[] args) {
		// Comment transformer un mot en tableau de caractere ?
		String mot = "allo";
		char[] aLettre = mot.toCharArray();
		// Afficher un tableau de caractere ?
		//System.out.println(aMot);
		
		// Comment transformer une phrase en tableau de mots ?
		String phrase = "coucou ca va";
		String[] aMot = phrase.split(" ");
		//for (String m : aMots) {System.out.println(m);}
		
		// Generer un nombre aleatoire
		int lettreMin = 0;
		int lettreMax = 0;
		int tire = 0;
		Random alea = new Random();
		tire = alea.nextInt(lettreMax - lettreMin + 1) + lettreMin; 
		
		// Cloner un tableau
		String[] aClone = aMot.clone();
		
		// Convertir un tableau de caracteres en String
		char[] aCaracteresAConvertir =  {'T', 'o', 'm', ' ', 'C', 'r', 'u', 'i', 's', 'e'};  
		String laChaine = String.valueOf(aCaracteresAConvertir);

		
		
		// On aura TOUJOURS besoin de la 1ere lettre
		lettreMin = 1;
		
		// Soit une phrase ...
		String phraseSource = "Comment quelle s appellait nom de dieu lulu la nantaise";
		
		// Ventilee facon puzzle dans un tableau
		String[] aMots = phraseSource.split(" ");
		
		// Copie de ce tableau pour pouvoir comparer avant / apres
		String[] aMotsCible = aMots.clone();
		
		// Examinons chaque mot
		for (int i = 0; i < aMots.length; i++) {
			//System.out.println(i + " : " + aMots[i]);
			// Longueur du mot
			int lMot = aMots[i].length();
			// Ya pas de ptites zeconomies, on ne manipule pas les mots de 3 lettres et moins
			if (lMot > 2) {
				// Preservation de la derniere lettre
				lettreMax = lMot - 2;
				// Copie du mot dans un tableau de caracteres
				char[] aLettres = aMots[i].toCharArray();
				// Preparation du tableau de caracteres cible
				char[] aLettresCible = new char[lMot];
				// 1ere et derniere lettre
				aLettresCible[0] = aLettres[0];
				aLettresCible[lMot - 1] = aLettres[lMot - 1];
				lettreMax = lMot - 2;
				//System.out.println("Pour " + String.valueOf(aLettres) + " lettreMax [" + lettreMax + "]");
				
				// shake baby shake
				int nbrModif = 0;
				//System.out.println(String.valueOf(aLettres) + " \t " + lettreMax);
				for (int j = lettreMin; j <= lettreMax; j++) {
					//System.out.println("Pour " + String.valueOf(aLettres) + ", lettre " + j + " " + aLettres[j]);
					do {
						int tirage = alea.nextInt(lettreMax + 1); 
						//System.out.println("Pour " + String.valueOf(aLettres) + ", lettre " + j + " " + aLettres[j] + "tirage " + tirage );
						if (aLettresCible[tirage] == '\0') {  //'\u0000'  '\0'
							aLettresCible[tirage] = aLettres[j];
							nbrModif++;
							break;
						}
					} while ( nbrModif < lettreMax);
				}

				// copie du tableau de caracteres vers la String du tableau de mots cible
				aMotsCible[i] = String.valueOf(aLettresCible);
			}
			else {
				aMotsCible[i] = aMots[i];
			}		
		}
		
		// Controle
		for (int i = 0; i < aMots.length; i++) {
			System.out.println(aMots[i] + " \t\t [" + aMotsCible[i] + "]");
		}
		
	}

}
