package controller;

import java.util.Scanner;

public class MainController {

	public static void main(String[] args) {
		// 18.5 < IMC < 25
		float IMC_BAS = 18.5f;
		float IMC_HAUT = 25f;
		float imc = 0f;
		float taille = 0f;
		float poids = 0f;
		
//		poids = 170f;
//		taille = 1.65f;
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Quel est votre poids (en kg) ?");
		poids = sc.nextFloat();
		System.out.println("Quelle est votre taille (en m) ?");
		taille = sc.nextFloat();
		//System.out.println("poids : [" + poids + "] \t\t taille : [" + taille + "]");
		
		imc =  poids / (taille * taille);
		
		if (imc < IMC_BAS) {
			System.out.println("IMC : " + (int) imc + " Attention par grand vent");
		}
		else if (imc > IMC_HAUT) {
			System.out.println("IMC : " + (int) imc + " Vous etes rassurant.");
			System.out.println("Je vous propose de perdre " 
					+ (int) ((taille * taille) * ((IMC_BAS + IMC_HAUT) / 2)) 
					+ " kg");
		}
		else {
			System.out.println("IMC : " + (int) imc + " Vous etes banalement dans la norme dictatrice.");
		}
		
		sc.close();
	}

}
